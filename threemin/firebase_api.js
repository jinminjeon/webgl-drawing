
FireBaseApi = function (session_id, your_guid, your_name, other_guid, other_name, addNewCanvas, deleteCanvas, OnRemoveLine) {

	var RootRef = new Firebase('https://sky-rtc-f8169.firebaseio.com/DrawingEx');

	// This token is only for admin. do not use it for commecial version!!!
	RootRef.authWithCustomToken('NlNnvOzbrJmUptrRlludpmO4yZtWhDWZIt5KPtQi', function(error, authData) {
  		if (error) {
    		console.log("Login Failed!", error);
  		} else {
    		console.log("Login Succeeded!", authData);
  		}
	});
	
	var UserURLRoot = 'https://sky-rtc-f8169.firebaseio.com/DrawingEx/session-users/';
	var UserRootRef = new Firebase(UserURLRoot);

	var SessionURLRoot = 'https://sky-rtc-f8169.firebaseio.com/DrawingEx/sessions/';
	var SessionRootRef = new Firebase(SessionURLRoot);
	var SessionID = session_id;
	var pageIDArray = [];
	var PagePathRefArrayQuickL = [];
	var PagePathRefArrayQuickR = [];
	
	var PagePathRefArrayAnimatedR = [];

	var YourGUID = your_guid;
	var YourName = your_name;
	var OtherGUID = other_guid;
	var OtherName = other_name;

	var localPositionArray = []; // for save pumping
	var currentPositionArray;

	var currentPathColorL;
	var currentPathColorR;

	var currentPathWidthL;
	var currentPathWidthR;

	var FPS = parseInt(1000  / 15);
	var minDrawBufferSize = 10;
	var curDrawBufferEndCount = 0;


	var pageCallback = addNewCanvas;
	var removeCallback = OnRemoveLine;
	var pageRemoveCallback = deleteCanvas;

	var UndoBuffer = [];
	var RedoBuffer = [];

//	console.log(pageCallback);

	var sessionRef = SessionRootRef.child(session_id);

	sessionRef.once("value", function(snap) {
  		// do some stuff once
  		console.log("initial data loaded!", snap.val() /*Object.keys(snap.val())*/);

  		if ( snap.val() == null) {
  			// craete new session room
  			createSession(YourGUID, YourName, OtherGUID, OtherName);
  		}
	});


	var pageRef = SessionRootRef.child(SessionID + '/' + 'pages');

	pageRef.on("child_added", function(snapshot, prevChildKey) {
		var newPost = snapshot.val();
		console.log('Previous Post ID:'  + prevChildKey);
		console.log('new post', snapshot.key(), newPost);
		//console.log(snapshot.key);		
		pageIDArray.push(snapshot.key());		

		pageRef.child(snapshot.key()).child("paths").on("child_added", on_path_added);
        pageRef.child(snapshot.key()).child("paths").on("child_removed", on_path_removed);

        pageCallback();
	});

	pageRef.on("child_removed", function(snapshot) {
		//var deletedPost = snapshot.val();
		//console.log(deletedPost.title + "' has been deleted");
		console.log('page deleted ' , snapshot.key());

		for ( i = 0; i < pageIDArray.length; i++ ) {
			if ( pageIDArray[i] == snapshot.key()) {
				pageRemoveCallback(i);
				pageIDArray.splice(i, 1);
				break;
			}
		}

		pageRef.child(snapshot.key()).child("paths").off("child_added", on_path_added);
        pageRef.child(snapshot.key()).child("paths").off("child_removed", on_path_removed);
	});

	var interval = setInterval(on_save_timer, FPS);

	/*function on_save_handler() {

		console.log('on_save_handler');
	}*/

	function on_save_timer () {

		if ( localPositionArray.length == 0 ) {
			//setTimeout(arguments.callee, 300);
			return;
		}

		var PositionArray = [];
		var pathRef;

		for (i = 0; i < localPositionArray.length; i++) {

			var array = localPositionArray[i];

			var data = array[1];
			pathRef = array[0];

			if ( data == '-1:-1') {
				PositionArray.push(array[1]);
				updatePath(array[0], PositionArray);
				endPath(array[0]);
				PositionArray = [];
			}
			else {
				PositionArray.push(array[1]);
				//updatePath(array[1], array[2]);
			}
		}

		updatePath(pathRef, PositionArray);

		localPositionArray = [];

		//setTimeout(on_save_handler, FPS);

		//console.log('on_save_timer');
	}

	function on_local_points_added(snapshot) {

		var pointSnapshot = snapshot/*.ref().child(snapshot.key())*/;
		var PathKey = snapshot.ref().parent().parent().key();
		//var ColorSnapshot = snapshot.ref().parent().parent().val('color');
		//var WidthSnapshot = snapshot.ref().parent().parent().val('width');
		var PageKey = snapshot.ref().parent().parent().parent().parent().key();
		
		var pointArray = [];
		var endFlag = 0;

		console.log(PathKey, PageKey);

		pointSnapshot.forEach(function(childSnapshot) {
//    		var key = childSnapshot.key();
//    		console.log(key);
    		var childData = childSnapshot.val();
    		console.log('on_local_points_added', childData);

    		pointArray.push(childData);

    		if ( childData == '-1:-1' || childData == "-1.000000:-1.000000") {
    			snapshot.ref().parent().off("child_added", on_local_points_added);
    			console.log('remove on_local_points_added listener');
    			endFlag = 1;
    			//break;
    		}
  		});

  		var PageNo = 0;

  		for ( i = 0; i < pageIDArray.length; i++) {

  			if ( pageIDArray[i] == PageKey) {
  				PageNo = i;
  				break;
  			}
  		}

  		//console.log(ColorSnapshot, WidthSnapshot);

		PagePathRefArrayQuickL.push([PageNo, PathKey, 0, endFlag, currentPathColorL, currentPathWidthL, pointArray]);
	}

	function on_remote_points_added(snapshot) {

		var pointSnapshot = snapshot/*.ref().child(snapshot.key())*/;
		var PathKey = snapshot.ref().parent().parent().key();
		var PageKey = snapshot.ref().parent().parent().parent().parent().key();
		var pointArray = [];
		var endFlag = 0;

		console.log(PathKey, PageKey);

		pointSnapshot.forEach(function(childSnapshot) {
//    		var key = childSnapshot.key();
//    		console.log(key);
    		var childData = childSnapshot.val();
    		//console.log('on_remote_points_added ' , childData);

    		pointArray.push(childData);

    		if ( childData == '-1:-1' || childData == "-1.000000:-1.000000") {
    			snapshot.ref().parent().off("child_added", on_remote_points_added);
    			console.log('remove on_remote_points_added listener');
    			endFlag = 1;
    			//break;
    		}
  		});

  		var PageNo = 0;

  		for ( i = 0; i < pageIDArray.length; i++) {

  			if ( pageIDArray[i] == PageKey) {
  				PageNo = i;
  				break;
  			}
  		}

		PagePathRefArrayQuickR.push([PageNo, PathKey, 0, endFlag, currentPathColorR, currentPathWidthR, pointArray]);
	}

	function on_points_added(snapshot) {

		var pointSnapshot = snapshot/*.ref().child(snapshot.key())*/;
		var PathKey = snapshot.ref().parent().parent().key();
		var PageKey = snapshot.ref().parent().parent().parent().parent().key();
		var pointArray = [];
		var endFlag = 0;

		//console.log(PathKey, PageKey);

		pointSnapshot.forEach(function(childSnapshot) {
//    		var key = childSnapshot.key();
//    		console.log(key);
    		var childData = childSnapshot.val();
    		
    		//console.log('on_points_added ' , childData);

    		pointArray.push(childData);

    		if ( childData == '-1:-1' || childData == "-1.000000:-1.000000") {
    			snapshot.ref().parent().off("child_added", on_points_added);
    			console.log('remove on_points_added listener');
    			endFlag = 1;
    			curDrawBufferEndCount++;
    			//break;
    		}
  		});

  		var PageNo = 0;

  		for ( i = 0; i < pageIDArray.length; i++) {

  			if ( pageIDArray[i] == PageKey) {
  				PageNo = i;
  				break;
  			}
  		}

		PagePathRefArrayAnimatedR.push([PageNo, PathKey, 0, endFlag, currentPathColorR, currentPathWidthR, pointArray]);
	}

	 
	function on_path_added(snapshot) {
		var pageKey = snapshot.ref().parent().parent().key();
		var pathRef = snapshot.ref();

		var PageNo = 0;

		for ( i = 0; i < pageIDArray.length; i++) {

  			if ( pageIDArray[i] == pageKey) {
  				PageNo = i;
  				break;
  			}
  		}

  		/*
		console.log(pageKey);
		console.log(snapshot.key());
		console.log(snapshot.val().color);
		console.log(snapshot.val().owner);
		console.log(snapshot.val().width);
		*/

		console.log('on_path_added ', snapshot.key());
		
		var pointSnapshot = snapshot.ref().child("points");

		//console.log('pointSnapshot ', pointSnapshot);

		if ( snapshot.val().isCompletePath == true) {

			if ( snapshot.val().owner == YourGUID ) {

				currentPathColorL = snapshot.val().color;
				currentPathWidthL = snapshot.val().width;

				PagePathRefArrayQuickL.push([PageNo, snapshot.key(), 1, 0, currentPathColorL, currentPathWidthL, null]);
				pointSnapshot.on("child_added", on_local_points_added);
			}
			else {

				console.log('on_path_added : remote quickdraw');
				currentPathColorR = snapshot.val().color;
				currentPathWidthR = snapshot.val().width;
				PagePathRefArrayQuickR.push([PageNo, snapshot.key(), 1, 0, currentPathColorR, currentPathWidthR, null]);
				pointSnapshot.on("child_added", on_remote_points_added);
			}
		}
		else {

			if ( snapshot.val().owner != YourGUID ) {
				
				currentPathColorR = snapshot.val().color;
				currentPathWidthR = snapshot.val().width;
				PagePathRefArrayAnimatedR.push([PageNo, snapshot.key(), 1, 0, currentPathColorR, currentPathWidthR, null]);
				pointSnapshot.on("child_added", on_points_added);
			}
		}

		/*
		pointSnapshot.forEach(function(childSnapshot) {
    		var key = childSnapshot.key();
    		console.log(key);
    		var childData = childSnapshot.val();
    		console.log(childData);
  		});
		*/

  		//if ( snapshot.val().owner != YourGUID )
  		//	remoteSnapShotArray.push(snapshot);
	}

	function on_path_removed(snapshot) {

		var pageKey = snapshot.ref().parent().parent().key();
		var PathKey = snapshot.key();

		console.log('on_path_remove', pageKey, PathKey);

		var PageNo = 0;

		for ( i = 0; i < pageIDArray.length; i++) {

  			if ( pageIDArray[i] == pageKey) {
  				PageNo = i;
  				break;
  			}
  		}

		removeCallback(PageNo, PathKey);
		
		//OnPathRemove(pageKey, sanpshot);
	}


	this.setUser = function (subURL, accept, badge, lastMessage, lastUpdate, requestFlag, roomID, title) {

		var usersRef = UserRootRef.child(subURL);

		usersRef.set({
	  		"acceptFlag" : accept,
	  		"badge" : badge,
	  		"lastMessage" : lastMessage,
	  		"lastUpdate" : lastUpdate,
	  		"requestFlag" : requestFlag,
	  		"roomID" : roomID,
	  		"sessionTitle" : title
		});
	}

	this.updateUser = function(subURL, key, value) {

		var usersRef = UserRootRef.child(subURL);

		usersRef.update({
	  		key : value
		});
	}

	function createSession (host_guid, host_name, guest_guid, guest_name) {

		var sessionRef = SessionRootRef.child(SessionID + '/');

		var hostRef = sessionRef.child('users/host/');
		var guestRef = sessionRef.child('users/guest/');

		hostRef.set({
			"guid" : host_guid,
			"hascontrol" : true,
			"name" : host_name,
			"presence" : false
		});

		guestRef.set({
			"guid" : guest_guid,
			"hascontrol" : false,
			"name" : guest_name,
			"presence" : false
		});
		
		var keyRef = pageRef.push({

			"isControllerWatching" : false,
			"owner" : host_guid,
			"paths" : ""
		});

		console.log('page ' + keyRef.key() + ' was created' )

		pageIDArray.push(keyRef.key());
	}

	this.addPage = function () {

		if ( pageIDArray.length >= 10) {
			console.log('maximum page reached!');
			return;
		}

		var keyRef = pageRef.push({

			"isControllerWatching" : false,
			"owner" : YourGUID,
			"paths" : ""
		});

		console.log('page ' + keyRef.key() + ' was created' )

		//pageIDArray.push(keyRef.key());	
	}

	this.removePage = function(pageNo) {

		if ( pageIDArray.length <= 0) {
			console.log('no page');
			return;
		}

		if ( pageNo >= pageIDArray.length || pageNo <= 0 ) {
			console.log('invalid page num', pageNo);
			return;	
		}

		var pageRef = SessionRootRef.child(SessionID + '/' + 'pages');
		pageRef.child(pageIDArray[pageNo]).remove();
	}

	var currentPathRef;
	//var currentPointRef;

	this.addNewPath = function (pageNo, pointArray, width, color) {

		var currentPath = pageRef.child(pageIDArray[pageNo]).child('paths');

		var PathRef = currentPath.push({
			"color" : color,
			//"isCompletePath" : false,
			"owner" : YourGUID,
			//"points" : "",
			"timestamp" : 0,
			"width" : width
		});

		var currentPathKey = PathRef.key();
		currentPathRef = currentPath.child(currentPathKey);

		var pointsRef = currentPath.child(currentPathKey).child('points');

		pointsRef.push([pointArray]);

		localPositionArray.push([currentPathRef, pointArray]);

		UndoBuffer.push([pageNo, currentPathKey, width, color, null]);
		currentPositionArray = [];
		currentPositionArray.push(pointArray);
		RedoBuffer = [];

		return currentPathKey;

		//console.log('addNewPath ', pointArray);
	}

	this.addPostion = function (pointArray) {

		localPositionArray.push([currentPathRef, pointArray]);
		currentPositionArray.push(pointArray);

		// end of position
		if (pointArray == '-1:-1') {
			var array = UndoBuffer[UndoBuffer.length-1];
			array[4] = currentPositionArray;

			UndoBuffer[UndoBuffer.length-1] = array;
			//console.log('UndoBuffer ', UndoBuffer);
		}
		//console.log('addPostion ', pointArray);
		//console.log('addPostion currentPointRef ', currentPathRef);
	}

	this.getQuickDrawingDataL = function() {

		var returnArray = PagePathRefArrayQuickL;
		PagePathRefArrayQuickL = [];

		return returnArray;
	}

	this.getQuickDrawingDataR = function() {

		var returnArray = PagePathRefArrayQuickR;
		PagePathRefArrayQuickR = [];

		return returnArray;
	}	

	this.getAnimatedDrawing = function() {

		if ( PagePathRefArrayAnimatedR.length == 0) {
			//console.log('getAnimatedDrawing is empty');
			return null;
		}

		if ( PagePathRefArrayAnimatedR.length < minDrawBufferSize && curDrawBufferEndCount == 0) {

			//console.log('getAnimatedDrawing buffering now!!');
			return null;
		}

		var returnArray = PagePathRefArrayAnimatedR[0];

		var endFlag = returnArray[3];

		if ( endFlag == 1 ) curDrawBufferEndCount--;

		PagePathRefArrayAnimatedR.splice(0, 1);

		return returnArray;
	}

	this.clearPage = function(PageNo) {

		var pageRef = SessionRootRef.child(SessionID + '/' + 'pages');
		pageRef.child(pageIDArray[PageNo]).child("paths").remove();
		RedoBuffer = [];
		UndoBuffer = [];
	}

	this.Undo = function(PageNo) {

		if ( UndoBuffer.length == 0 ) return;
		var index;

		for (index = UndoBuffer.length - 1; index >= 0; index--) {

			var temp = UndoBuffer[index];
			if ( temp[0] == PageNo) break;
		}

		if ( index < 0 ) return;

		var UndoArray = UndoBuffer[index];

		var pageRef = SessionRootRef.child(SessionID + '/' + 'pages');

		pageRef.child(pageIDArray[UndoArray[0]]).child("paths").child(UndoArray[1]).remove();
		RedoBuffer.unshift(UndoArray);
		UndoBuffer.splice(index, 1);
		console.log('UndoBuffer ', UndoBuffer);
	}

	this.Redo = function(pageNo) {

		if ( RedoBuffer.length == 0) return;

		var RedoArray = RedoBuffer[0];

		var currentPath = pageRef.child(pageIDArray[RedoArray[0]]).child('paths');

		var PathRef = currentPath.push({
			"color" : RedoArray[3],
			//"isCompletePath" : false,
			"owner" : YourGUID,
			"isCompletePath" : true,
			//"points" : "",
			"timestamp" : 0,
			"width" : RedoArray[2]
		});

		var currentPathKey = PathRef.key();
		var pointsRef = currentPath.child(currentPathKey).child('points');
		
		pointsRef.push(RedoArray[4]);

		RedoArray[1] = currentPathKey;

		UndoBuffer.push(RedoArray);
		RedoBuffer.splice(0, 1);
		console.log('UndoBuffer ', UndoBuffer);
	}

	function updatePath(pathRef, pointArray) {

		var pointsRef = pathRef.child('points');
		pointsRef.push(pointArray);

		//console.log(pointRef, pointArray);
	}

	function endPath(PathRef) {

//		var childRef = pageRef.child(pageIDArray[pageNo]).child('paths');
		PathRef.update (
		{
			"isCompletePath" : true
		});

		console.log('endPath ');
	}
}