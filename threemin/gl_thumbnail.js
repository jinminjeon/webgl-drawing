
// Our Javascript will go here.
WebGLThumbNail = function ( PosX, PosY, width, height, OnCallback, Param ) {
	
	var scene = new THREE.Scene();
	var DrawingWidth = width;
	var DrawingHeight = height;
	var OnPage = OnCallback;
	var PageNum = Param;

	var camera = new THREE.PerspectiveCamera( 90, DrawingWidth / DrawingHeight, 1, 1000 );
	camera.position.z = 31;

		
	var renderer = new THREE.CanvasRenderer();

	var ImageMesh = null;
	var ambientLight = null;
	var directionalLight = null;
	
	renderer.setClearColor( 0xf0f0f0 );
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( DrawingWidth, DrawingHeight );
	renderer.autoScaleCubemaps = false;

	renderer.domElement.style.position = 'absolute';
    renderer.domElement.style.top = PosY;
    renderer.domElement.style.left = PosX;

	document.body.appendChild( renderer.domElement );

	renderer.domElement.addEventListener('resize', onWindowResize, false );
	renderer.domElement.addEventListener( 'mousedown', onMouseDown, false );

	render();
			
	function render() {
		renderer.render( scene, camera );
	}

	function onWindowResize () {

		camera.aspect = DrawingWidth / DrawingHeight;
		camera.updateProjectionMatrix();

		renderer.setSize( DrawingWidth, DrawingHeight );
		render();
	}


	function onMouseDown( event ) {
		OnPage(PageNum);
	}

	this.SetHide = function (flag)
	{
		if ( flag == true )
			renderer.domElement.style.display="none";
		else renderer.domElement.style.display="";
	}

	this.deallocateRender = function() {
		renderer = null;
	}

	this.DoSync = function (imgUrl) {
		

		/*
		material = new THREE.MeshBasicMaterial( {
        color: "white",
      	} );

		 var callback = function() {
         // to be called after the texture is loaded, to show the new texture
     		material.needsUpdate = true;

            render();
       	};
		
	    var texture = THREE.ImageUtils.loadTexture(imgUrl, undefined, callback);
	    material.map = texture; // Applies this texture to the material and hence to the object.
   		material.needsUpdate = true;  // Essential to tell three.js that material properties have changed!!

	    
	      // cube
	    var cube = new THREE.Mesh(new THREE.PlaneGeometry(DrawingWidth, DrawingHeight), material);
	    cube.overdraw = true;

	    scene.add(cube);

	     // add subtle ambient lighting
	    if ( ambientLight == null) {
			ambientLight = new THREE.AmbientLight(0xbbbbbb);
			scene.add(ambientLight);
		}

		// directional lighting

		if ( directionalLight == null ) {
			directionalLight = new THREE.DirectionalLight(0xffffff);
			directionalLight.position.set(1, 1, 1).normalize();
			scene.add(directionalLight);
		}

		if ( ImageMesh != null ) {
			scene.remove(ImageMesh);
		}

		ImageMesh = cube;
		*/

//		console.log(renderer.domElement);
//		console.log(imgUrl);

		var image 	= new Image();
		image.src = imgUrl;

		var canvas = renderer.domElement;
		var ctx		= canvas.getContext('2d');
		ctx.fillStyle	= "black";
		ctx.fillRect(0, 0, canvas.width, canvas.height);

		ctx.drawImage(image, 0, 0, canvas.width, canvas.height );

		//console.log(image);
	}
}


/*
function ClassName(w, h) {
	this.prop1 = "";
	this.prop2 = "";
}

ClassName.prototype = {
	method1: function() {

	},
	method2: function() {

	}	
};


var cls = new ClassName(800, 600);
*/