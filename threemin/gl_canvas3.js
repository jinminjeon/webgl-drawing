// Our Javascript will go here.
WebGLCanvas = function ( PosX, PosY, width, height, OnDrawStart, OnDrawing, PageNo ) {
	
	THREE.ImageUtils.crossOrigin = "";
	var scene = new THREE.Scene();
	var imageScene = new THREE.Scene();

	var DrawingWidth = width;
	var DrawingHeight = height;
	var leftPos = PosX;
	var topPos = PosY;
	var DrawStartCallback = OnDrawStart
	var DrawingCallback = OnDrawing
	var PageNumber = PageNo;

	var camera = new THREE.PerspectiveCamera( 90, DrawingWidth / DrawingHeight, 1, 10000 );
	camera.position.z = 200;

	var virtualCamera = new THREE.PerspectiveCamera( 90, DrawingWidth / DrawingHeight, 1, 10000 );
	virtualCamera.position.z = 200;

	var textCamera = new THREE.PerspectiveCamera( 90, DrawingWidth / DrawingHeight, 1, 10000 );
	

	var prevVirtualPos;
	var isMouseDown = 0;	
	var prevMesh2;

	var previousPointR;
	var previousMidPointR;
	var stripArrayR;
	var prevMeshR;
	var meshArrayR = [];

	var mouseClicked = 0;
	var LineWidth = 0.01;
	var LineColor = 0x000000;
	var Opacity = 1;
	var Fov = camera.fov;
	var Zoom = 1.0;
	var ZoomInc = -0.02;
	var GuestureMode = 0;
	var renderer = new THREE.WebGLRenderer( {antialias: true, preserveDrawingBuffer: true } );
	var meshArray = [];
	var localFbaseKey;
	var remoteFbaseKey;
	var redoBuffer = [];

	var ImageMesh = null;
	var ambientLight = null;
	var directionalLight = null;
	
	var CamLimitR = 0;
	var CamLimitL = 0;
	var CamLimitT = 0;
	var CamLimitB = 0;

	var previousPoint;
	var previousMidPoint;
	var stripArray;
	var DrawText;
	var	TextOptions;
	var	TextColor;

	var meshForRay = []; // [[meshobject, [position array], ...]
	var pickedMesh = null;

//	var projector = new THREE.Projector();
//	var raycaster = new THREE.Raycaster();

	renderer.setClearColor( 'lightgrey' );
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( DrawingWidth, DrawingHeight );
	renderer.autoScaleCubemaps = false;
	renderer.autoClear = false;

	renderer.domElement.style.position = 'absolute';
    renderer.domElement.style.top = PosY;
    renderer.domElement.style.left = PosX;

	document.body.appendChild( renderer.domElement );

	renderer.domElement.addEventListener('resize', onWindowResize, false );
	renderer.domElement.addEventListener( 'mousemove', onMouseMove, false );
	renderer.domElement.addEventListener( 'mousedown', onMouseDown, false );
	renderer.domElement.addEventListener( 'mouseup', onMouseUp, false );
	renderer.domElement.addEventListener( 'mouseout', onMouseOut, false);
	renderer.domElement.addEventListener( 'mouseenter', onMouseIn, false);

	//initFXAA();

	/*
	var vertices = [];
	var segments = 20;
	for(var i = 0; i <= segments; i++){
		var u = ( i / segments ) * Math.PI * 2 - Math.PI;
		var v = Math.cos( 2 * u );
		
		vertices.push(new THREE.Vector3( 10 * u, 3 * v + 15, 0 ));
	}
	
	// nonbuffered line strip
	var geometry = new THREE.Geometry();
	for(var i = 0; i < vertices.length; i++){
		geometry.vertices.push(vertices[i]);
	}



	var line = new THREE.Line( geometry, new THREE.LineBasicMaterial({color: 0xff0000, linewidth :10}));
	line.mode = THREE.LineStrip;
	line.defaultColor = 0xff0000;
	scene.add(line);
	*/

	render();

	var needThumbnailUpdate = false;

	var vMousePos = getMousePosition(0,0);
	
	var GlMax = getMousePosition(DrawingWidth+leftPos, DrawingHeight+topPos);
	
	var convX = DrawingWidth / 2.000000 / GlMax.x;
	//realPos = glPOS * convX + (width/2)
	var convY = DrawingHeight / 2.000000 / GlMax.y;

	console.log('convX', convX, GlMax.x);

	//realPos = glPOS * convY + (height/2)

	function initFXAA()
	{
		dpr = 1;
		if (window.devicePixelRatio !== undefined) {
  			dpr = window.devicePixelRatio;
		}

		renderScene = new THREE.RenderPass(scene, camera);
		effectFXAA = new THREE.ShaderPass(THREE.FXAAShader);
		effectFXAA.uniforms['resolution'].value.set(1 / (DrawingWidth * dpr), 1 / (DrawingHeight * dpr));
		effectFXAA.renderToScreen = true;

		composer = new THREE.EffectComposer(renderer);
		composer.setSize(DrawingWidth * dpr, DrawingHeight * dpr);
		composer.addPass(renderScene);
		composer.addPass(effectFXAA);

		return;
	}

	function DrawLineByRemote(width, color, opacity) {

		var curveGeometry = Line()(stripArrayR, vMousePos.z);

		// material
		var material = new THREE.ShaderMaterial( 

			BasicShader({
		    	side: THREE.DoubleSide ,
		    	diffuse: color,
		    	opacity : opacity,
		    	thickness: width
			})			
		 );

		material.transparent = true;
		 
		var mesh = new THREE.Mesh(curveGeometry, material);
		scene.add(mesh);

		if ( prevMeshR )
			scene.remove(prevMeshR);

		prevMeshR = mesh;

		render();
	}


	//this.DrawLineByRemote = function(array, width, color, opacity, complete) {

	this.DrawLineByRemoteMid = function (rarray, width, color, opacity) {


		//console.log('DrawLineByRemoteMid', rarray, width, color, opacity);

		if ( previousPointR != null) {

			var distance = Math.sqrt((rarray[0] - previousPointR[0]) * 
				(rarray[0] - previousPointR[0]) + (rarray[1] - previousPointR[1]) * (rarray[1] - previousPointR[1]));
			var mid = [];

			mid[0] = (rarray[0] + previousPointR[0]) / 2.0;
			mid[1] = (rarray[1] + previousPointR[1]) / 2.0;
					
			var i;
			var segments =  parseInt(distance / 1.5);
			var v;
			
			if ( segments > 0 ) {
						
				for (i = 0; i < segments; i++)
				{
					var array = QuadraticPointInCurve(previousMidPointR, mid, previousPointR, i / segments);
					v = getMousePositionRemote(array[0], array[1]);
					stripArrayR.push([v.x, v.y]);
				}

				DrawLineByRemote(width, color, opacity);
			}
		}
		else {

			previousPointR = [];
			previousMidPointR = [];		
			previousPointR = [rarray[0], rarray[1]];
			previousMidPointR = [rarray[0], rarray[1]];
			v = getMousePositionRemote(rarray[0], rarray[1]);

			//stripArrayR = [];
			stripArrayR.push([v.x, v.y]);
			return;
		}
		
		previousPointR[0] = rarray[0];
		previousPointR[1] = rarray[1];
		previousMidPointR = mid;
	}

	this.DrawLineByRemoteStart = function(owner, fbaseID) {

		console.log('remote line start', owner, fbaseID);

		prevMeshR = null;

		previousPointR = null;
		previousMidPointR = null;

		stripArrayR = [];

		if ( owner == 1 ) localFbaseKey = fbaseID;
		else remoteFbaseKey = fbaseID;
	}

	this.DrawLineByRemoteEnd = function(owner)
	{
		console.log('remote line complete', owner);

		if ( prevMeshR ) {

			if ( owner == 1 ) meshArray.push([localFbaseKey, prevMeshR]);
			else meshArrayR.push([remoteFbaseKey, prevMeshR]);
			
			prevMeshR = null;
		}

		needThumbnailUpdate = true;
	}

	// this function can be used  remote object only
	this.RemoveLine = function(lineKey) {
		
		var lmesh = null;
		var rmesh = null;

		for ( i = 0; i < meshArrayR.length; i++) {

			var element = meshArrayR[i];

			if ( element[0] == lineKey ) {
				rmesh = element[1];
				meshArrayR.splice(i, 1);
				break;
			}
		}

		if ( rmesh == null )
		{
			for ( i = 0; i < meshArray.length; i++) {

				var element = meshArray[i];

				if ( element[0] == lineKey ) {
					lmesh = element[1];
					meshArray.splice(i, 1);
					break;
				}
			}
		}

		if ( lmesh == null && rmesh == null ) {
			console.log('removeline error!! invalid pathKey : ', lineKey);
			return;
		}

		var i;
		for( i = scene.children.length - 1; i >= 0; i--) { 
			
			obj = scene.children[i];

			if (obj == lmesh) {

				scene.remove(obj);
				render();
				needThumbnailUpdate = true;
				return;
			}
			else if (obj == rmesh) {
				scene.remove(obj);
				render();
				needThumbnailUpdate = true;
				return;
			}
		}
	}

	//var PolyLine = null;

	function DrawVertextLine4(cameraZ) {

		var curveGeometry = Line()(stripArray, cameraZ);


		//console.log(stripArray);

		// material
		var material = new THREE.ShaderMaterial( 

			BasicShader({
		    	side: THREE.DoubleSide ,
		    	diffuse: LineColor,
		    	opacity : Opacity,
		    	thickness: LineWidth
			})
		 );

		material.transparent = true;
		 
		var mesh = new THREE.Mesh(curveGeometry, material);
		scene.add(mesh);

		if ( prevMesh2 )
			scene.remove(prevMesh2);

		prevMesh2 = mesh;

		render();
	}

			
	function render() {
		
		renderer.clear();
		renderer.render( imageScene, camera );
		renderer.clearDepth();
		renderer.render( scene, camera );

		//renderer.clear();
		//composer.render();
	}

	function onWindowResize () {

		camera.aspect = DrawingWidth / DrawingHeight;
		camera.updateProjectionMatrix();

		renderer.setSize( DrawingWidth, DrawingHeight );

		effectFXAA.uniforms['resolution'].value.set(1 / (DrawingWidth * dpr), 1 / (DrawingHeight * dpr));
  		composer.setSize(DrawingWidth * dpr, DrawingHeight * dpr);

  		render();
	}

	function getScroll() {

		if( window.pageYOffset!= undefined) {
			return [pageXOffset, pageYOffset];
		}
		else {
			var sx, sy, d= document, r= d.documentElement, b= d.body;
			sx= r.scrollLeft || b.scrollLeft || 0;
			sy= r.scrollTop || b.scrollTop || 0;
			return [sx, sy];
		}
	}

	function getMousePositionRemote(clientX, clientY) {

		var RealX = clientX;
		var RealY = clientY;

	    var mouse2D = new THREE.Vector3();
	    var mouse3D = new THREE.Vector3();
	    mouse2D.x = ( RealX  / DrawingWidth) * 2 - 1;
	    mouse2D.y = -(RealY /DrawingHeight) * 2 + 1;
	    mouse2D.z = 0.5;

	    mouse3D = mouse2D.unproject(camera);
	    return mouse3D;    
	}

	function getMousePosition(clientX, clientY) {

		var scroll = getScroll();

		var RealX = clientX - leftPos + scroll[0];
		var RealY = clientY - topPos + scroll[1];

	    var mouse2D = new THREE.Vector3();
	    var mouse3D = new THREE.Vector3();
	    mouse2D.x = ( RealX  / DrawingWidth) * 2 - 1;
	    mouse2D.y = -(RealY /DrawingHeight) * 2 + 1;
	    mouse2D.z = 0.5;
		
		//console.log('getMousePosition', mouse2D);

	    mouse3D = mouse2D.unproject(camera);
	    return mouse3D;
	}
	
	function getOriginalPosition(glX, glY) {

		var x = glX * convX + (DrawingWidth / 2.000000);
		var y = glY * convY + (DrawingHeight / 2.000000);

		//console.log('getOriginalPosition',  x, y, glX, glY);
		
		if ( x < 0) x = 0;
		if ( x > DrawingWidth) x = DrawingWidth;
		if ( y < 0) y = 0;
		if ( y > DrawingHeight) y = DrawingHeight;
		
		
				
		return [x,y];
	}

	function getVirtualMousePosition (clientX, clientY) {

		var scroll = getScroll();

		var RealX = clientX - leftPos + scroll[0];
		var RealY = clientY - topPos + scroll[1];

	    var mouse2D = new THREE.Vector3();
	    var mouse3D = new THREE.Vector3();
	    mouse2D.x = (RealX / DrawingWidth) * 2 - 1;
	    mouse2D.y = -(RealY /DrawingHeight) * 2 + 1;
	    mouse2D.z = 0.5;

	    mouse3D = mouse2D.unproject(virtualCamera);
	    return mouse3D;
	}

	function onMouseOut ( event ) {
		console.log('onMouseOut');
	}

	function onMouseIn ( event ) {
		console.log('onMouseIn');

		if ( event.which == 0 && prevMesh2 != null )
		{			
			meshArray.push(prevMesh2);
			prevMesh2 = null;
			console.log('onMouseIn addMesh', meshArray);
		}
	}

	function QuadraticPointInCurve(start, end, controlPoint, percent) {

		var a = Math.pow((1.0 - percent), 2.0);
		var b = 2.0 * percent * (1.0 - percent);
		var c = Math.pow(percent, 2.0);
		var Pos = [];

		Pos[0] = a * start[0] + b * controlPoint[0] + c * end[0];
		Pos[1] = a * start[1] + b * controlPoint[1] + c * end[1];

		return Pos;
	}
	
	function onMouseMove ( event ) {

		//event.preventDefault();

		//console.log(event.clientX + ' ' + event.clientY);

		if ( event.which == 0 ) 
		{
			return;
		}
		
		if ( isMouseDown == 0 ) 
		{
			return;
		}
		
		var mouse;

		if ( GuestureMode == 1 )
		{
			mouse = getVirtualMousePosition(event.clientX, event.clientY);

			var posX = camera.position.x - (mouse.x - prevVirtualPos.x);
			var posY = camera.position.y - (mouse.y - prevVirtualPos.y);

			console.log('posX ' +  posX);
			console.log('posY ' +  posY);

			
			if ( posX <= CamLimitL || posX >= CamLimitR ) 
			{
				console.log('camera LR limit');
			}
			else camera.position.x -= (mouse.x - prevVirtualPos.x);

			if ( posY <= CamLimitB || posY >= CamLimitT )
			{
				console.log('camera TB limit');
			}
			else camera.position.y -= (mouse.y - prevVirtualPos.y);
						
			console.log(camera.position);
			prevVirtualPos = mouse;
			render();
			return;
		}
		else if ( GuestureMode == 2 )
			return;
		else if ( GuestureMode == 3 )
		{
			DrawRectangleMid(event.clientX, event.clientY);
			return;
		}
		else if ( GuestureMode == 4 )
		{
			if ( pickedMesh != null ) {

				var objType = pickedMesh[0];
				var oldMesh = pickedMesh[1];
				var options = pickedMesh[3];

				//console.log(pickedMesh);

				if ( objType == 'textBox') {

					pickedMesh = UpdateDrawTextMesh(event.clientX, event.clientY, oldMesh, options  );
				}
				else if ( objType == 'rectangle')  {

					pickedMesh = UpdateRectangle(event.clientX, event.clientY, oldMesh, options  );
				}
			}

			return;
		}


		var scroll = getScroll();
		
		if ( Zoom != 1.0 ) {
			
			glPos = getMousePosition(event.clientX, event.clientY);
			OriginPos = getOriginalPosition(glPos.x, glPos.y);
			
			console.log('fase send in zoom', OriginPos);
			
			var relativeX = (OriginPos[0]) / DrawingWidth;
			var relativeY = (OriginPos[1]) / DrawingHeight;
			
			//relativeMousePosArray.push((relativeX).toPrecision(6) + ":" + (relativeY).toPrecision(6));

			DrawingCallback((relativeX).toPrecision(6) + ":" + (relativeY).toPrecision(6));
			
		}
		else {
			var relativeX = (event.clientX - leftPos + scroll[0]) / DrawingWidth;
			var relativeY = (event.clientY - topPos + scroll[1]) / DrawingHeight;
			
			DrawingCallback((relativeX).toPrecision(6) + ":" + (relativeY).toPrecision(6));
		}

		if ( previousPoint != null) {

			var distance = Math.sqrt((event.clientX - previousPoint[0]) * 
				(event.clientX - previousPoint[0]) + (event.clientY - previousPoint[1]) * (event.clientY - previousPoint[1]));
			var mid = [];

			mid[0] = (event.clientX + previousPoint[0]) / 2.0;
			mid[1] = (event.clientY + previousPoint[1]) / 2.0;
					
			var i;
			var segments =  parseInt(distance / 1.5);
			//var segments = parseInt(distance * 2);
			var v;
			//var quadArray = [];
			
			if ( segments > 0 ) {
						
				for (i = 0; i < segments; i++)
				{
					var array = QuadraticPointInCurve(previousMidPoint, mid, previousPoint, i / segments);
					v = getMousePosition(array[0], array[1]);
					stripArray.push([v.x, v.y]);
				}

				DrawVertextLine4(v.z);
			}
		}
		
		previousPoint = [];
		previousPoint[0] = event.clientX;
		previousPoint[1] = event.clientY;
		previousMidPoint = mid;
	}

	function onMouseDown( event ) {

		isMouseDown = 1;

		//PickingObject(event.clientX, event.clientY);
		
		/*
		glPos = getMousePosition(event.clientX, event.clientY);
		OriginPos = getOriginalPosition(glPos.x, glPos.y);

		console.log('onMouseDown GlPOS',  glPos);
		console.log('onMouseDown OriginPOS',  OriginPos);
		*/


		if ( GuestureMode == 1)
		{
			prevVirtualPos = getVirtualMousePosition(event.clientX, event.clientY);
			return;
		}
		else if ( GuestureMode == 2 )
		{
			DrawTextMesh(event.clientX, event.clientY);
			return;
		}
		else if ( GuestureMode == 3 )
		{
			DrawRectangleStart(event.clientX, event.clientY);
			return;
		}
		else if ( GuestureMode == 4)
		{
			pickedMesh = PickingObject(event.clientX, event.clientY);
			return;
		}

		//quadratic = createQuadraticBuilder();
		
		prevMesh2 = null;

		previousPoint = [];
		previousMidPoint = [];
		
		previousPoint = [event.clientX, event.clientY];
		previousMidPoint = [event.clientX, event.clientY];
		v = getMousePosition(event.clientX, event.clientY);

		stripArray = [];
		stripArray.push([v.x, v.y]);
	
		redoBuffer = [];

		var scroll = getScroll();
		
		if ( Zoom != 1.0 ) {
			
			glPos = getMousePosition(event.clientX, event.clientY);
			OriginPos = getOriginalPosition(glPos.x, glPos.y);
			
			var relativeX = (OriginPos[0]) / DrawingWidth;
			var relativeY = (OriginPos[1]) / DrawingHeight;
			
			//relativeMousePosArray.push((relativeX).toPrecision(6) + ":" + (relativeY).toPrecision(6));

			localFbaseKey = DrawStartCallback(PageNumber, (relativeX).toPrecision(6) + ":" + (relativeY).toPrecision(6));
			
		}
		else {
			var relativeX = (event.clientX - leftPos + scroll[0]) / DrawingWidth;
			var relativeY = (event.clientY - topPos + scroll[1]) / DrawingHeight;
			
			//relativeMousePosArray.push((relativeX).toPrecision(6) + ":" + (relativeY).toPrecision(6));

			localFbaseKey = DrawStartCallback(PageNumber, (relativeX).toPrecision(6) + ":" + (relativeY).toPrecision(6));
		}
	}

	function onMouseUp ( event ) {

//		console.log('onMouseUp')

		isMouseDown = 0;

		if ( GuestureMode == 3 )
		{
			DrawRectangleFinal(event.clientX, event.clientY);
			return;
		}
		else if ( GuestureMode == 4 )
		{
			if ( pickedMesh != null ) {

				var objType = pickedMesh[0];
				var oldMesh = pickedMesh[1];
				var options = pickedMesh[3];

				//console.log(pickedMesh);

				if ( objType == 'textBox') {

					UpdateDrawTextMesh(event.clientX, event.clientY, oldMesh, options  );
				}
				else if ( objType == 'rectangle')  {

					UpdateRectangle(event.clientX, event.clientY, oldMesh, options  );
				}


				pickedMesh = null;
			}

			return;
		}
		else if ( GuestureMode != 0 ) return;

//		if ( GuestureMode != 0 ) return;

		if ( prevMesh2 ) {
			meshArray.push([localFbaseKey, prevMesh2]);
			//meshForRay.push(prevMesh2);
			prevMesh2 = null;
			console.log(meshArray);
		}

		//previousThickness = 0;
		//penThickness = 0;

		var scroll = getScroll();
		
		if ( Zoom != 1.0 ) {
			
			glPos = getMousePosition(event.clientX, event.clientY);
			OriginPos = getOriginalPosition(glPos.x, glPos.y);
									
			var relativeX = (OriginPos[0]) / DrawingWidth;
			var relativeY = (OriginPos[1]) / DrawingHeight;
			
			//relativeMousePosArray.push((relativeX).toPrecision(6) + ":" + (relativeY).toPrecision(6));

			DrawingCallback((relativeX).toPrecision(6) + ":" + (relativeY).toPrecision(6));
			
		}
		else {
			var relativeX = (event.clientX - leftPos + scroll[0]) / DrawingWidth;
			var relativeY = (event.clientY - topPos + scroll[1]) / DrawingHeight;

			DrawingCallback((relativeX).toPrecision(6) + ":" + (relativeY).toPrecision(6));
		}
		
        DrawingCallback('-1:-1');

		needThumbnailUpdate = true;

		// DrawLineByRemoteEnd();
	}

	this.OnZoomIn = function () {

		Zoom += ZoomInc;

		if ( Zoom <= 0 )
		{
			console.log('Max zoon-in');
			return;
		}

		camera.fov = Fov * Zoom;
		camera.updateProjectionMatrix();
		render();



		var vFOV = camera.fov * Math.PI / 180;        // convert vertical fov to radians
		var height = 2 * Math.tan( vFOV / 2 ) * (DrawingHeight / 2); // visible height

		var aspect = DrawingWidth / DrawingHeight;
		var width = height * aspect;

		console.log('width ' + width + 'height' + height);

		var invWidth = DrawingWidth - width;
		var invHeight = DrawingHeight - height;

		console.log('width ' + invWidth + 'height ' + invHeight);

		mouse1 = getVirtualMousePosition(0, 0);
		mouse2 = getVirtualMousePosition(invWidth, invHeight);

		var RL = (mouse2.x - mouse1.x) * 0.5;
		var BT = -(mouse2.y - mouse1.y) * 0.5;

		console.log('RL ' + RL + 'BT ' + BT);

		CamLimitR = RL;
		CamLimitL = -RL;
		CamLimitB = -BT;
		CamLimitT = BT;

		console.log('CamLimitR ' + CamLimitR);
		console.log('CamLimitL ' + CamLimitL);
		console.log('CamLimitT ' + CamLimitT);
		console.log('CamLimitB ' + CamLimitB);
	}

	this.OnZoomOut = function () {

		if ( Zoom == 1.0) return;
		Zoom -= ZoomInc;
		camera.fov = Fov * Zoom;
		camera.updateProjectionMatrix();
		render();

		var vFOV = camera.fov * Math.PI / 180;        // convert vertical fov to radians
		var height = 2 * Math.tan( vFOV / 2 ) * 100; // visible height

		var aspect = DrawingWidth / DrawingHeight;
		var width = height * aspect;

		console.log('width ' + width + 'height' + height);

		var invWidth = DrawingWidth - width;
		var invHeight = DrawingHeight - height;

		console.log('width ' + invWidth + 'height ' + invHeight);

		mouse1 = getVirtualMousePosition(0, 0);
		mouse2 = getVirtualMousePosition(invWidth, invHeight);

		var RL = (mouse2.x - mouse1.x);
		var BT = -(mouse2.y - mouse1.y);

		console.log('RL ' + RL + 'BT ' + BT);
		
		CamLimitR = -RL;
		CamLimitL = +RL;
		CamLimitB = +BT;
		CamLimitT = -BT;		
	}

	this.OnZoomNormal = function () {
				
		Zoom = 1.0;
		camera.fov = Fov;		
		camera.position.x = 0;
		camera.position.y = 0;
		camera.updateProjectionMatrix();
		render();
	
		CamLimitR = 0;
		CamLimitL = 0;
		CamLimitT = 0;
		CamLimitB = 0;

		needThumbnailUpdate = true;

		/*
		mouse1 = getMousePosition(DrawingWidth, DrawingHeight);
		mouse2 = getMousePosition(0, 0);

		CamLimitR = mouse1.x / 3.0;
		CamLimitL = mouse2.x / 3.0;
		CamLimitT = mouse2.y / 3.0;
		CamLimitB = mouse1.y / 3.0;

		var vFOV = camera.fov * Math.PI / 180;        // convert vertical fov to radians
		var height = 2 * Math.tan( vFOV / 2 ) * 300; // visible height

		var aspect = DrawingWidth / DrawingHeight;
		var width = height * aspect;  

		console.log('width ' + width + 'height' + height);
		*/
	}

	this.OnClear = function () {

		var i;
		for( i = scene.children.length - 1; i >= 0; i--) { 
			obj = scene.children[i];

			if ( obj != ImageMesh && obj != ambientLight && obj != directionalLight  )
				scene.remove(obj);
		}

		meshArray = [];
		meshArrayR = [];
		meshForRay = [];
		redoBuffer = [];
		render();
		needThumbnailUpdate = true;
	}

	this.Undo = function ()
	{
		if ( meshArray.length == 0 ) return;

		var i = meshArray.length - 1;		
		redo = meshArray[i];

		redoBuffer.unshift(redo);
		scene.remove(redo[1]);
		meshArray.splice(i, 1);

		render();
		//renderer.domElement.toDataURL();
		needThumbnailUpdate = true;
	}

	this.Redo = function ()
	{
		if ( redoBuffer.length == 0) return;

		var redo = redoBuffer[0];
		meshArray.push(redo);

		scene.add(redo[1]);
		redoBuffer.splice(0, 1);

		render();
		needThumbnailUpdate = true;
	}

	this.SetLineColor = function (color) {
		LineColor = color;
	}

	this.GetLineColor = function () {
		return LineColor;
	}

	this.SetLineWidth = function (width) {
		LineWidth = width;
		strokeWidthMin = LineWidth;
		strokeWidthMax = strokeWidthMin * 5;
	}

	this.GetLineWidth = function () {

		return LineWidth;
	}

	this.SetOpacity = function(flag) {
		Opacity = flag;
	}

	this.GetOpacity = function(flag) {

		return Opacity;
	}

	this.SetCanvasMode = function (flag) {
		GuestureMode = flag;
	}

	this.SetHide = function (flag)
	{
		if ( flag == true )
			renderer.domElement.style.display="none";
		else renderer.domElement.style.display="";
	}

	this.SetBackgroundImage = function (imgUrl, width, height) {
		
		// Create Light
		var light = new THREE.PointLight(0xFFFFFF);
		light.position.set(0, 0, 500);
		scene.add(light);

		// Create Ball
		var vertShader = document.getElementById('vertexShader').innerHTML;
		var fragShader = document.getElementById('fragmentShader').innerHTML;

		 var callback = function() {
         // to be called after the texture is loaded, to show the new texture
            render();
       	};		

		var uniforms = {
    		texture1: { type: "t", value: THREE.ImageUtils.loadTexture(imgUrl, undefined, callback) }
		};

		var material = new THREE.ShaderMaterial({
		uniforms: uniforms,
		side: THREE.DoubleSide ,
		vertexShader: vertShader,
		fragmentShader: fragShader
		});
		
		var box = new THREE.BoxGeometry( width * 1.716, height * 1.716, 1 );
		//var geometry = new THREE.BufferGeometry().fromGeometry(box);


		for ( i = 0; i < box.vertices.length; i++) {
			//console.log(box.vertices[i]);
			box.vertices[i].x /= -vMousePos.z;
			box.vertices[i].y /= vMousePos.z;
			box.vertices[i].z = vMousePos.z;
			//console.log(box.vertices[i]);
		}

		var cube = new THREE.Mesh(box, material)
		imageScene.add(cube);
		//console.log(box);


		if ( ImageMesh != null ) {
			scene.remove(ImageMesh);
		}

		ImageMesh = cube;

		render();

		needThumbnailUpdate = true;
	}

	this.GetScreenShot = function() {

		needThumbnailUpdate = false;
		return renderer.domElement.toDataURL();
	}

	this.needUpdate = function() {
		return needThumbnailUpdate;
	}

	this.getPostionArray = function() {

		return relativeMousePosArray;
	}

	this.deallocateRender = function() {
		renderer = null;
	}

	this.setTextMode = function(text, size, color) {

		GuestureMode = 2;
		DrawText = text;
		TextOptions = size;
		TextColor = color;
	}

	function getTextMousePos(clientX, clientY) {
		
		var scroll = getScroll();

		var RealX = clientX - leftPos + scroll[0];
		var RealY = clientY - topPos + scroll[1];

	    var mouse2D = new THREE.Vector3();
	    var mouse3D = new THREE.Vector3();
	    mouse2D.x = (RealX / DrawingWidth) * 2 - 1;
	    mouse2D.y = -(RealY /DrawingHeight) * 2 + 1;
	    mouse2D.z = 0.5;

	    mouse3D = mouse2D.unproject(textCamera);
	    return mouse3D;
	}

	function UpdateDrawTextMesh(mouseX, mouseY, oldMesh, options) {

		var text = options[0];
		var textsize = options[1];
		var textcolor = options[2];

		var     canvas,
        context,
        metrics = null,
        textHeight = 25,
        textWidth = 0,
        actualFontSize = textsize;

        canvas = document.createElement('canvas'),
        context = canvas.getContext('2d');
        //context.fillStyle = /*'#0000FF'*/;

    // 2d duty
		metrics = context.measureText(text);
		var textWidth = metrics.width * actualFontSize * 10;
		textHeight = textsize * 100;

		//console.log('canvas', textWidth, textHeight);

		canvas.width = textWidth;
		canvas.height = textHeight;
		context.font = "normal " + textHeight + "px Arial";
		context.textAlign = "center";
		context.textBaseline = "middle";
		context.fillStyle = textcolor;//"#0000FF";
		context.fillText(text, textWidth / 2, textHeight / 2);

		var texture = new THREE.Texture(canvas);
		texture.needsUpdate = true;
		texture.minFilter = THREE.LinearFilter;
		var material = new THREE.SpriteMaterial({ map: texture, useScreenCoordinates: false });
		var sprite = new THREE.Sprite( material );

		// find scence object
		// find meshForRay
		var glWidth = textWidth / textHeight * actualFontSize/2;
		var glHeight = actualFontSize/2;

		sprite.scale.set(glWidth, glHeight, 1);

		//console.log(sprite);

  		glPos = getMousePosition(mouseX, mouseY);

		//console.log(sprite);

		sprite.position.x = glPos.x;
		sprite.position.y = glPos.y; 
		sprite.position.z = glPos.z;

		for( i = scene.children.length - 1; i >= 0; i--) { 
			obj = scene.children[i];

			if ( obj == oldMesh  )
				scene.children[i] = sprite;
		}

		//scene.add(sprite);

		//meshArray.push([localFbaseKey, sprite]);

		var raystartX = glPos.x - glWidth / 2;
		var raystartY = glPos.y + glHeight / 2;
		var rayendX = glPos.x + glWidth / 2;
		var rayendY = glPos.y - glHeight / 2;

		PosLT = getOriginalPosition(raystartX, raystartY);
		//console.log('TextBox Origin', PosLT);

		PosRB = getOriginalPosition(rayendX, rayendY);
		//console.log('TextBox Edge', PosRB);

		var rayObject = ['textBox', sprite, [PosLT[0], PosLT[1], PosRB[0], PosRB[1]], [DrawText, TextOptions, TextColor] ];

		for ( i = 0; i < meshForRay.length; i++) {

			var obj = meshForRay[i];
			var mesh = obj[1];

			if ( mesh == oldMesh ) meshForRay[i] = rayObject;
		}

		render();

	 	return rayObject;
	}


	//	{'font' : 'helvetiker','weight' : 'normal', 'style' : 'normal','size' : 100,'curveSegments' : 300};
	function DrawTextMesh (mouseX, mouseY) {

		var     canvas,
        context,
        metrics = null,
        textHeight = 25,
        textWidth = 0,
        actualFontSize = TextOptions;
        
        canvas = document.createElement('canvas'),
        context = canvas.getContext('2d');
        //context.fillStyle = /*'#0000FF'*/;

    // 2d duty
		metrics = context.measureText(DrawText);
		var textWidth = metrics.width * actualFontSize * 10;
		//textHeight = metrics.height * actualFontSize * 10;
		//var textWidth = DrawingWidth * 2;
		textHeight = TextOptions * 100;

		console.log('canvas', textWidth, textHeight);

		canvas.width = textWidth;
		canvas.height = textHeight;
		context.font = "normal " + textHeight + "px Arial";
		context.textAlign = "center";
		context.textBaseline = "middle";
		context.fillStyle = TextColor;//"#0000FF";
		context.fillText(DrawText, textWidth / 2, textHeight / 2);

		var texture = new THREE.Texture(canvas);
		texture.needsUpdate = true;
		var material = new THREE.SpriteMaterial({ map: texture, useScreenCoordinates: false });
		var sprite = new THREE.Sprite( material );


		//var textObject = new THREE.Object3D();
		// var sprite = new THREE.Sprite(texture);
		//textObject.textHeight = actualFontSize;
		//textObject.textWidth = (textWidth / textHeight) * textObject.textHeight;
		var glWidth = textWidth / textHeight * actualFontSize/2;
		var glHeight = actualFontSize/2;

		sprite.scale.set(glWidth, glHeight, 1);

		console.log(sprite);

  		glPos = getMousePosition(mouseX, mouseY);

		//console.log(sprite);

		sprite.position.x = glPos.x;
		sprite.position.y = glPos.y; 
		sprite.position.z = glPos.z;

		scene.add(sprite);

		meshArray.push([localFbaseKey, sprite]);

		var raystartX = glPos.x - glWidth / 2;
		var raystartY = glPos.y + glHeight / 2;
		var rayendX = glPos.x + glWidth / 2;
		var rayendY = glPos.y - glHeight / 2;

		PosLT = getOriginalPosition(raystartX, raystartY);
		console.log('TextBox Origin', PosLT);

		PosRB = getOriginalPosition(rayendX, rayendY);
		console.log('TextBox Edge', PosRB);

		meshForRay.push(['textBox', sprite, [PosLT[0], PosLT[1], PosRB[0], PosRB[1]], [DrawText, TextOptions, TextColor] ]);

		render();
	}


	var RectangleStartX, RectangleStartY;

	function DrawRectangleStart(mouseX, mouseY) {

		glPos = getMousePosition(mouseX, mouseY);
		RectangleStartX = glPos.x;
		RectangleStartY = glPos.y;
		prevMesh2 = null;
	}

	function DrawRectangleMid(mouseX, mouseY) {

		glPos = getMousePosition(mouseX, mouseY);

		var rectWidth = Math.abs(glPos.x - RectangleStartX);
		var rectHeight = Math.abs(glPos.y -RectangleStartY );
		var Radius;

		if ( rectWidth > rectHeight ) {
			Radius = rectHeight / 4;
		}
		else {
			Radius = rectHeight / 4;
		}

		if ( Radius > 0.1 ) Radius = 0.1;

		//console.log(rectWidth, rectHeight, Radius);

		var rectShape = new THREE.Shape();

		( function roundedRect( ctx, x, y, width, height, radius ){

			ctx.moveTo( x, y + radius );
			ctx.lineTo( x, y + height - radius );
			ctx.quadraticCurveTo( x, y + height, x + radius, y + height );
			ctx.lineTo( x + width - radius, y + height) ;
			ctx.quadraticCurveTo( x + width, y + height, x + width, y + height - radius );
			ctx.lineTo( x + width, y + radius );
			ctx.quadraticCurveTo( x + width, y, x + width - radius, y );
			ctx.lineTo( x + radius, y );
			ctx.quadraticCurveTo( x, y, x, y + radius );

		} )( rectShape, RectangleStartX, RectangleStartY - rectHeight, rectWidth, rectHeight, Radius );
		
		
		/*

		var x = RectangleStartX;
		var y = RectangleStartY - rectHeight;

		rectShape.moveTo( x, y );
		rectShape.lineTo( x, y + rectHeight );
		rectShape.lineTo( x + rectWidth, y + rectHeight );
		rectShape.lineTo( x + rectWidth, y );
		rectShape.lineTo( x, y );
		*/

		var points = rectShape.createPointsGeometry();

		//console.log(points);

		var pointArray = [];

		for ( i = 0; i < points.vertices.length; i++) {
			pointArray.push([points.vertices[i].x, points.vertices[i].y]);
		}
		//var spacedPoints = rectShape.createSpacedPointsGeometry( 200 );

		// solid line
		//var lineMesh = new THREE.Line( points, new THREE.LineBasicMaterial( { color: 0xFF0000, linewidth: 10 } ) );

		//console.log(pointArray);


		var curveGeometry = Line()(pointArray, vMousePos.z);

		// material
		var material = new THREE.ShaderMaterial( 

			BasicShader({
		    	side: THREE.DoubleSide ,
		    	diffuse: LineColor,
		    	opacity : Opacity,
		    	thickness: LineWidth
			})			
		 );

		material.transparent = true;

		lineMesh = new THREE.Mesh(curveGeometry, material);
		scene.add(lineMesh); 
		
		if ( prevMesh2 )
			scene.remove(prevMesh2);

		prevMesh2 = lineMesh;

		render();
	}

	function DrawRectangleFinal(mouseX, mouseY) {

		if ( prevMesh2 )
			scene.remove(prevMesh2);
		
		prevMesh2 = null;

		glPos = getMousePosition(mouseX, mouseY);

		var rectWidth = Math.abs(glPos.x - RectangleStartX);
		var rectHeight = Math.abs(glPos.y -RectangleStartY );
		var Radius;

		if ( rectWidth > rectHeight ) {
			Radius = rectHeight / 4;
		}
		else {
			Radius = rectHeight / 4;
		}

		if ( Radius > 0.1 ) Radius = 0.1;

		console.log(RectangleStartX, RectangleStartY, rectWidth, rectHeight, Radius);

		var rectShape = new THREE.Shape();

		( function roundedRect( ctx, x, y, width, height, radius ){

			ctx.moveTo( x, y + radius );
			ctx.lineTo( x, y + height - radius );
			ctx.quadraticCurveTo( x, y + height, x + radius, y + height );
			ctx.lineTo( x + width - radius, y + height) ;
			ctx.quadraticCurveTo( x + width, y + height, x + width, y + height - radius );
			ctx.lineTo( x + width, y + radius );
			ctx.quadraticCurveTo( x + width, y, x + width - radius, y );
			ctx.lineTo( x + radius, y );
			ctx.quadraticCurveTo( x, y, x, y + radius );

		} )( rectShape, RectangleStartX, RectangleStartY - rectHeight, rectWidth, rectHeight, Radius );
		
		
		/*

		var x = RectangleStartX;
		var y = RectangleStartY - rectHeight;

		rectShape.moveTo( x, y );
		rectShape.lineTo( x, y + rectHeight );
		rectShape.lineTo( x + rectWidth, y + rectHeight );
		rectShape.lineTo( x + rectWidth, y );
		rectShape.lineTo( x, y );
		*/

		var points = rectShape.createPointsGeometry();

		//console.log(points);

		var pointArray = [];

		for ( i = 0; i < points.vertices.length; i++) {
			pointArray.push([points.vertices[i].x, points.vertices[i].y]);
		}
		//var spacedPoints = rectShape.createSpacedPointsGeometry( 200 );

		// solid line
		//var lineMesh = new THREE.Line( points, new THREE.LineBasicMaterial( { color: 0xFF0000, linewidth: 10 } ) );

		//console.log(pointArray);


		var curveGeometry = Line()(pointArray, vMousePos.z);

		// material
		var material = new THREE.ShaderMaterial( 

			BasicShader({
		    	side: THREE.DoubleSide ,
		    	diffuse: LineColor,
		    	opacity : Opacity,
		    	thickness: LineWidth
			})
		 );

		material.transparent = true;

		lineMesh = new THREE.Mesh(curveGeometry, material);

		scene.add(lineMesh); 
		meshArray.push([localFbaseKey, lineMesh]);

		PosLT = getOriginalPosition(RectangleStartX, RectangleStartY);
		console.log('Rectangle Origin', PosLT);

		PosRB = getOriginalPosition(glPos.x, glPos.y);

		console.log('Rectangle Edge', PosRB);
			
		meshForRay.push(['rectangle', lineMesh, [PosLT[0], PosLT[1], PosRB[0], PosRB[1]], 
			[rectWidth, rectHeight, Radius, LineColor, Opacity, LineWidth ]]);

		render();
	}

	function UpdateRectangle(mouseX, mouseY, oldMesh, options) {

		var rectWidth = options[0];
		var rectHeight = options[1];
		var Radius = options[2];

		glPos = getMousePosition(mouseX, mouseY);
		var RectangleStartX = glPos.x - rectWidth / 2;
		var RectangleStartY = glPos.y + rectHeight / 2;
		var RectangleEndX = glPos.x + rectWidth / 2;
		var RectangleEndY = glPos.y - rectHeight / 2;

		var rectShape = new THREE.Shape();

		( function roundedRect( ctx, x, y, width, height, radius ){

			ctx.moveTo( x, y + radius );
			ctx.lineTo( x, y + height - radius );
			ctx.quadraticCurveTo( x, y + height, x + radius, y + height );
			ctx.lineTo( x + width - radius, y + height) ;
			ctx.quadraticCurveTo( x + width, y + height, x + width, y + height - radius );
			ctx.lineTo( x + width, y + radius );
			ctx.quadraticCurveTo( x + width, y, x + width - radius, y );
			ctx.lineTo( x + radius, y );
			ctx.quadraticCurveTo( x, y, x, y + radius );

		} )( rectShape, RectangleStartX, RectangleStartY - rectHeight, rectWidth, rectHeight, Radius );
		
		var points = rectShape.createPointsGeometry();

		//console.log(points);

		var pointArray = [];

		for ( i = 0; i < points.vertices.length; i++) {
			pointArray.push([points.vertices[i].x, points.vertices[i].y]);
		}
		
		var curveGeometry = Line()(pointArray, vMousePos.z);

		// material
		var material = new THREE.ShaderMaterial( 

			BasicShader({
		    	side: THREE.DoubleSide ,
		    	diffuse: options[3],
		    	opacity : options[4],
		    	thickness: options[5]
			})
		 );

		material.transparent = true;

		lineMesh = new THREE.Mesh(curveGeometry, material);

		for( i = scene.children.length - 1; i >= 0; i--) { 
			obj = scene.children[i];

			if ( obj == oldMesh  )
				scene.children[i] = lineMesh;
		}

//		meshArray.push([localFbaseKey, lineMesh]);

		PosLT = getOriginalPosition(RectangleStartX, RectangleStartY);
		console.log('Rectangle Origin', PosLT);

		PosRB = getOriginalPosition(RectangleEndX, RectangleEndY);

		console.log('Rectangle Edge', PosRB);
			
		var rayObject = ['rectangle', lineMesh, [PosLT[0], PosLT[1], PosRB[0], PosRB[1]], [rectWidth, rectHeight, Radius, LineColor, Opacity, LineWidth ]];

		for ( i = 0; i < meshForRay.length; i++) {

			var obj = meshForRay[i];
			var mesh = obj[1];

			if ( mesh == oldMesh ) meshForRay[i] = rayObject;
		}

		render();

		return rayObject;
	}

	function PickingObject(mouseX, mouseY) {

		glPos = getMousePosition(mouseX, mouseY);
		OriginPos = getOriginalPosition(glPos.x, glPos.y);

		var realX = OriginPos[0];
		var realY = OriginPos[1];

		for ( i = meshForRay.length - 1; i >= 0; i-- ) {

			var object = meshForRay[i];
			var objectMesh = object[1];
			var objectPos = object[2];

			//console.log(objectPos, mouseX, mouseY);

			if ( realX >= objectPos[0] && realX <= objectPos[2] && realY >= objectPos[1] && realY <= objectPos[3]) {

				console.log(object[0], objectMesh)
				return meshForRay[i];
			}
		}

		return null;
	}
}


/*
function ClassName(w, h) {
	this.prop1 = "";
	this.prop2 = "";
}

ClassName.prototype = {
	method1: function() {

	},
	method2: function() {

	}	
};


var cls = new ClassName(800, 600);
*/