
var VERTS_PER_POINT = 2
var tmp = [0, 0]
var lineA = [0, 0]
var lineB = [0, 0]
var tangent = [0, 0]
var miter = [0, 0]

function add(out, a, b) {
    out[0] = a[0] + b[0]
    out[1] = a[1] + b[1]
    return out
}

function set(out, x, y) {
    out[0] = x
    out[1] = y
    return out
}

function normalize(out, a) {
    var x = a[0],
        y = a[1]
    var len = x*x + y*y
    if (len > 0) {
        //TODO: evaluate use of glm_invsqrt here?
        len = 1 / Math.sqrt(len)
        out[0] = a[0] * len
        out[1] = a[1] * len
    }
    return out
}

function subtract(out, a, b) {
    out[0] = a[0] - b[0]
    out[1] = a[1] - b[1]
    return out
}

function dot(a, b) {
    return a[0] * b[0] + a[1] * b[1]
}

function util() {

    util.prototype.computeMiter = function (tangent, miter, lineA, lineB, halfThick) {
        //get tangent line
        add(tangent, lineA, lineB)
        normalize(tangent, tangent)

        //get miter as a unit vector
        set(miter, -tangent[1], tangent[0])
        set(tmp, -lineA[1], lineA[0])

        //get the necessary length of our miter
        return halfThick / dot(miter, tmp)
    }

    util.prototype.normal = function(out, dir) {
        //get perpendicular
        set(out, -dir[1], dir[0])
        return out
    }

    util.prototype.direction = function(out, a, b) {
        //get unit dir of two lines
        subtract(out, a, b)
        normalize(out, out)
        return out
    }
}

var util = new util();
  
function getNormals(points, closed) {
    var curNormal = null
    var out = []
    if (closed) {
        points = points.slice()
        points.push(points[0])
    }

    var total = points.length
    for (var i=1; i<total; i++) {
        var last = points[i-1]
        var cur = points[i]
        var next = i<points.length-1 ? points[i+1] : null

        util.direction(lineA, cur, last)
        if (!curNormal)  {
            curNormal = [0, 0]
            util.normal(curNormal, lineA)
        }

        if (i === 1) //add initial normals
            addNext(out, curNormal, 1)

        if (!next) { //no miter, simple segment
            util.normal(curNormal, lineA) //reset normal
            addNext(out, curNormal, 1)
        } else { //miter with last
            //get unit dir of next line
            util.direction(lineB, next, cur)

            //stores tangent & miter
            var miterLen = util.computeMiter(tangent, miter, lineA, lineB, 1)

            if ( i > 1 )
            {
                var prevMiterLen = out[i-1];
                if ( miterLen > 2 ) miterLen = prevMiterLen[1];
            }

            addNext(out, miter, miterLen)
        }
    }

    //if the polyline is a closed loop, clean up the last normal
    if (points.length > 2 && closed) {
        var last2 = points[total-2]
        var cur2 = points[0]
        var next2 = points[1]

        util.direction(lineA, cur2, last2)
        util.direction(lineB, next2, cur2)
        util.normal(curNormal, lineA)
        
        var miterLen2 = util.computeMiter(tangent, miter, lineA, lineB, 1)
        out[0][0] = miter.slice()
        out[total-1][0] = miter.slice()
        out[0][1] = miterLen2
        out[total-1][1] = miterLen2
        out.pop()
    }

    //console.log(out);

    return out
}

function addNext(out, normal, length) {
    out.push([[normal[0], normal[1]], length])
}


  // implementation from standard node.js 'util' module
function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    ctor.prototype = Object.create(superCtor.prototype, {
      constructor: {
        value: ctor,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
}

function Line(path, cameraZ, opt) {

function LineMesh(path, cameraZ, opt) {
        if (!(this instanceof LineMesh))
        {
            //console.log('create new line object');
            return new LineMesh(path, cameraZ, opt)
        }
        THREE.BufferGeometry.call(this)

        if (Array.isArray(path)) {
            opt = opt||{}
        } else if (typeof path === 'object') {
            opt = path
            path = []
        }

        opt = opt||{}

        this._positions = new THREE.BufferAttribute(null, 3)
        this._normals = new THREE.BufferAttribute(null, 2)
        this._miters = new THREE.BufferAttribute(null, 1)
        this._indices = new THREE.BufferAttribute(null, 1)

        if (opt.distances) 
            this._distances = new THREE.BufferAttribute(null, 1)

        this.update(path, opt.closed, cameraZ)

        this.addAttribute('position', this._positions)
        this.addAttribute('lineNormal', this._normals)
        this.addAttribute('lineMiter', this._miters)
        this.addAttribute('index', this._indices)

        if (opt.distances)
            this.addAttribute('lineDistance', this._distances)
    }

    inherits(LineMesh, THREE.BufferGeometry)
    
    LineMesh.prototype.update = function(path, closed, cameraZ) {

       // console.log('update');

        path = path||[]
        var normals = getNormals(path, closed)

        if (closed) {
            path = path.slice()
            path.push(path[0])
            normals.push(normals[0])
        }
        
        if (!this._positions.array ||
            (path.length !== this._positions.array.length/3/VERTS_PER_POINT)) {
            var count = path.length * VERTS_PER_POINT
            this._positions.array = new Float32Array(count * 3)
            this._normals.array = new Float32Array(count * 2)
            this._miters.array = new Float32Array(count * 1)
            this._indices.array = new Uint16Array(Math.max(0, (path.length-1) * 6))

            if (this._distances)
                this._distances.array = new Float32Array(count * 1)
        }
        var useDist = Boolean(this._distances)

        this._positions.needsUpdate = true
        this._miters.needsUpdate = true
        this._normals.needsUpdate = true
        this._indices.needsUpdate = true
        if (useDist)
            this._distances.needsUpdate = true
        
        var index = 0,
            c = 0, 
            dIndex = 0,
            indexArray = this._indices.array
            
        path.forEach(function(point, pointIndex, self) {
            var i = index
            indexArray[c++] = i + 0 
            indexArray[c++] = i + 1 
            indexArray[c++] = i + 2 
            indexArray[c++] = i + 2 
            indexArray[c++] = i + 1 
            indexArray[c++] = i + 3 

            this._positions.setXYZ(index++, point[0], point[1], cameraZ)
            this._positions.setXYZ(index++, point[0], point[1], cameraZ)

            if (useDist) {
                var d = pointIndex/(self.length-1)
                this._distances.setX(dIndex++, d)
                this._distances.setX(dIndex++, d)
            }
        }, this)

        var nIndex = 0, 
            mIndex = 0
        normals.forEach(function(n) {
            var norm = n[0]
            var miter = n[1]
            var prevMiter = 0;


            if ( miter > 2 || miter < -2 ) {
                if ( prevMiter > 0 ) miter = prevMiter;
                else miter = 1;
            }
            prevMiter = n[1];

            this._normals.setXY(nIndex++, norm[0], norm[1])
            this._normals.setXY(nIndex++, norm[0], norm[1])

            this._miters.setX(mIndex++, -miter)
            this._miters.setX(mIndex++, miter)
        }, this)

        //console.log(this._positions);
        //console.log(this._normals);
        //console.log(this._miters);
    }

    return LineMesh;
}